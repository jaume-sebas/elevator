/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LogicElevator;

/**
 *
 * @author sebass
 */

public class Elevator {
    public enum Action {
        UP,
        DOWN,
        STOPPED,
        OPENED;
    }
    public enum Perception {
        
    }
    
    public void buttonPressedInside(int button){}
    public Action getAction(){return Action.UP;}
    public void elapseTime(){}
    public void buttonPressedOutsideUp(int button){}
    public void buttonPressedOutsideDown(int button){}
    public int nextFloorAscending(){return 0;}
    public int nextFloorDescending(){return 0;}
}
