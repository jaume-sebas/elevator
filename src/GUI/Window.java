/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 *
 * @author Jaime
 */
public class Window extends JFrame{
    
    private ElevatorView elevator;
    
    public Window() {
        super.setBounds(100, 100, 1000, 950);
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        elevator = new ElevatorView();
        JScrollPane panelPane = new JScrollPane(elevator, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        panelPane.setAutoscrolls(true);
        panelPane.setPreferredSize(new Dimension (600, 600));
       
        add(panelPane);
        
    }
    
    
}
