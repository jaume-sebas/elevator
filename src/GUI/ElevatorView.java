/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import sun.java2d.loops.DrawRect;

/**
 *
 * @author Jaime
 */
public class ElevatorView extends JPanel {

    private JButton firstUp;
    private JButton firstDown;

    private JButton secondUp;
    private JButton secondDown;

    private ArrayList<JButton> upButtons;
    private ArrayList<JButton> downButtons;
    private ArrayList<JButton> paneButtons;

    private int NUM_FLOORS = 5;

    private int INITIAL_X_PANE_BUTTONS = 500;
    private int INITIAL_Y_PANE_BUTTONS = 100;
    private int AREA_PANE_BUTTON = 50;
    private int MIN_HEIGHT_BOX_PANE_BUTTON = 50;
    private int MIN_WIDTH_BOX_PANE_BUTTON = 50;
    private int MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON = 4;

    private int INITIAL_X_BUILDING = 100;
    private int INITIAL_Y_BUILDING = 10;
    private int WIDTH_BUILDING = 200;
    private int HEIGHT_BUILDING = 190 * NUM_FLOORS; //725

    private int INITIAL_X_ELEVATOR = 125;
    private int INITIAL_Y_ELEVATOR = 37;
    private int WIDTH_ELEVATOR = 150;
    private int HEIGHT_ELEVATOR = 150;

    private int INITIAL_X_UP_BUTTON = INITIAL_X_BUILDING + WIDTH_BUILDING + 30;
    private int INITIAL_Y_UP_BUTTON = 75;
    private int INITIAL_X_DOWN_BUTTON = INITIAL_X_UP_BUTTON;
    private int INITIAL_Y_DOWN_BUTTON = INITIAL_Y_UP_BUTTON + 50;
    private final int WIDTH_BUTTON = 30;
    private final int HEIGHT_BUTTON = 30;
    private final int BUTTON_SEPARATION = 175;
    
    // ------------ ELEVATOR --------------
    private int actual_floor = 0;
    
    // ------------------------------------

    public ElevatorView() {
        setLayout(null);
        setBounds(0, 0, 500, 1000);
        upButtons = new ArrayList<JButton>();
        downButtons = new ArrayList<JButton>();
        paneButtons = new ArrayList<JButton>();

        initComponents();

    }

    private void initComponents() {
        // ------------ Prepare image icon -----------
        Image iconUp = null;
        Image iconDown = null;
        try {
            iconUp = ImageIO.read(getClass().getResource("resources/up.png"));
            iconUp = iconUp.getScaledInstance(WIDTH_BUTTON, HEIGHT_BUTTON, java.awt.Image.SCALE_SMOOTH);

            iconDown = ImageIO.read(getClass().getResource("resources/down.png"));
            iconDown = iconDown.getScaledInstance(WIDTH_BUTTON, HEIGHT_BUTTON, java.awt.Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            Logger.getLogger(ElevatorView.class.getName()).log(Level.SEVERE, null, ex);
        }
        // -------------------------------------------
        // -------------------------------------------

        for (int i = 0; i < NUM_FLOORS; i++) {
            JButton paneButton = new JButton(i + "");
            paneButton.setBounds(INITIAL_X_PANE_BUTTONS + ((i % MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON) * MIN_WIDTH_BOX_PANE_BUTTON), INITIAL_Y_PANE_BUTTONS + ((i / MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON) * MIN_HEIGHT_BOX_PANE_BUTTON), AREA_PANE_BUTTON, AREA_PANE_BUTTON);
            paneButtons.add(paneButton);
            paneButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int new_floor = Integer.parseInt(paneButton.getText());
                    System.out.println(new_floor);
                    actual_floor = Integer.parseInt(paneButton.getText());
                    repaint();
                }
            });
            add(paneButtons.get(i));
        }

        for (int i = 0; i < NUM_FLOORS; i++) {
            JButton upButton = new JButton("UP");
            upButton.setBounds(INITIAL_X_UP_BUTTON, INITIAL_Y_UP_BUTTON + (i * BUTTON_SEPARATION), WIDTH_BUTTON, HEIGHT_BUTTON);
            upButton.setIcon(new ImageIcon(iconUp));
            upButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    upButton.setBackground(Color.GREEN);
                }
            });
            upButtons.add(upButton);
            add(upButtons.get(i));
        }

        for (int i = 0; i < NUM_FLOORS; i++) {
            JButton downButton = new JButton("DOWN");
            downButton.setBounds(INITIAL_X_DOWN_BUTTON, INITIAL_Y_DOWN_BUTTON + (i * BUTTON_SEPARATION), WIDTH_BUTTON, HEIGHT_BUTTON);
            downButton.setIcon(new ImageIcon(iconDown));
            downButtons.add(downButton);
            downButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    downButton.setBackground(Color.GREEN);
                }
            });
            add(downButtons.get(i));
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        paintBuilding(g2);
        paintButtonsPane(g2);
        paintElevator(g2);

    }

    private void paintBuilding(Graphics2D g2) {

        g2.setColor(Color.GRAY);
        g2.fillRect(INITIAL_X_BUILDING, INITIAL_Y_BUILDING, WIDTH_BUILDING, HEIGHT_BUILDING);
        g2.drawRect(INITIAL_X_BUILDING, INITIAL_Y_BUILDING, WIDTH_BUILDING, HEIGHT_BUILDING);
        g2.setColor(Color.BLACK);
        g2.drawRect(INITIAL_X_BUILDING - 1, INITIAL_Y_BUILDING - 1, WIDTH_BUILDING + 1, HEIGHT_BUILDING + 1);
    }

    private void paintButtonsPane(Graphics2D g2) {

        int num_buttons_in_width = (NUM_FLOORS > MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON ? MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON : NUM_FLOORS);

        g2.setColor(Color.GRAY);
        g2.fillRect(INITIAL_X_PANE_BUTTONS, INITIAL_Y_PANE_BUTTONS, MIN_WIDTH_BOX_PANE_BUTTON * num_buttons_in_width, MIN_HEIGHT_BOX_PANE_BUTTON * ((NUM_FLOORS / MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON) + 1));
        g2.drawRect(INITIAL_X_PANE_BUTTONS, INITIAL_Y_PANE_BUTTONS, MIN_WIDTH_BOX_PANE_BUTTON * num_buttons_in_width, MIN_HEIGHT_BOX_PANE_BUTTON * ((NUM_FLOORS / MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON) + 1));
        g2.setColor(Color.BLACK);
        g2.drawRect(INITIAL_X_PANE_BUTTONS - 1, INITIAL_Y_PANE_BUTTONS - 1, MIN_WIDTH_BOX_PANE_BUTTON * num_buttons_in_width + 1, MIN_HEIGHT_BOX_PANE_BUTTON * ((NUM_FLOORS / MAX_NUMBER_BUTTONS_IN_WIDTH_IN_PANE_BUTTON) + 1) + 1);

    }

    private void paintElevator(Graphics2D g2) {
        g2.setColor(Color.RED);

//        for (int i = 0; i < NUM_FLOORS; i++) {
//            g2.fillRect(INITIAL_X_ELEVATOR, INITIAL_Y_ELEVATOR + (i * 175), WIDTH_ELEVATOR, HEIGHT_ELEVATOR);
//            g2.drawRect(INITIAL_X_ELEVATOR, INITIAL_Y_ELEVATOR + (i * 175), WIDTH_ELEVATOR, HEIGHT_ELEVATOR);
//        }

        g2.fillRect(INITIAL_X_ELEVATOR, INITIAL_Y_ELEVATOR + (actual_floor * 175) , WIDTH_ELEVATOR, HEIGHT_ELEVATOR);
        g2.drawRect(INITIAL_X_ELEVATOR, INITIAL_Y_ELEVATOR + (actual_floor * 175) , WIDTH_ELEVATOR, HEIGHT_ELEVATOR);
        repaint();
    }
}